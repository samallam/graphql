/*
 * @(#)com.metlife.mutations.AccountSchemaMutations.java
 * ===========================================================================
 * This source code is exclusive propriety of the  Finances. 
 * In no case are the contents allowed to be distributed to third parties or made public 
 * without prior and written consent of Finances.
 * ===========================================================================
 *  
 */
/*
 * Change Activity:
 *
 * Reason    Date       Author         Version     Description
 * ------    ----       ------      -------     -------------------------------------- 
 * created  10-Feb-2016 Amit Mishra       0.1       created the artifact
 * 
 */
package com.metlife.mutations;


import graphql.relay.Edge;
import graphql.schema.*;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.metlife.model.Account;
import com.metlife.schema.AccountSchema;

import static graphql.Scalars.GraphQLBoolean;
import static graphql.Scalars.GraphQLID;
import static graphql.Scalars.GraphQLString;
import static graphql.schema.GraphQLFieldDefinition.newFieldDefinition;
import static graphql.schema.GraphQLInputObjectField.newInputObjectField;
/**
 * <code> AccountSchemaMutations <code> class
 * 
 * @version 0.1
 * @author Amit Mishra , IBM Corp.
 *
 */
public class AccountSchemaMutations {


    private GraphQLFieldDefinition addTodo;
    private GraphQLFieldDefinition changeStatus;
    private GraphQLFieldDefinition markAll;
    private GraphQLFieldDefinition removeCompleted;
    private GraphQLFieldDefinition removeTodo;
    private GraphQLFieldDefinition renameTodo;

    private AccountSchema todoSchema;


    public AccountSchemaMutations(AccountSchema todoSchema) {
        this.todoSchema = todoSchema;

        createAddTodoMutation();
    }

    private GraphQLFieldDefinition getViewerField() {
        GraphQLFieldDefinition viewer = newFieldDefinition()
                .name("viewer")
                .type(todoSchema.getUserType())
                .staticValue(todoSchema.getTheOnlyUser())
                .build();
        return viewer;
    }

    public List<GraphQLFieldDefinition> getFields() {
        return Arrays.asList(addTodo, changeStatus, markAll, removeCompleted, removeTodo, renameTodo);
    }


    private void createAddTodoMutation() {
        GraphQLInputObjectField textField = newInputObjectField()
                .name("text")
                .type(new GraphQLNonNull(GraphQLString))
                .build();

        List<GraphQLInputObjectField> inputFields = Arrays.asList(textField);

        GraphQLFieldDefinition todoEdge = newFieldDefinition()
                .name("accounts")
                .type(todoSchema.getAccountsEdge())
                .dataFetcher(environment -> {
                    Map source = (Map) environment.getSource();
                    String accountId = (String) source.get("id");
                    Account account = todoSchema.getAccount(accountId);
                    return new Edge(account, todoSchema.getSimpleConnection().cursorForObjectInConnection(account));
                })
                .build();


        List<GraphQLFieldDefinition> outputFields = Arrays.asList(todoEdge, getViewerField());

        DataFetcher mutate = environment -> {
            Map<String, Object> input = environment.getArgument("input");
            String text = (String) input.get("text");
            Account account = (Account)input;
            String newId = todoSchema.addAccount(account,null);
            Map<String, String> result = new LinkedHashMap<>();
            result.put("clientMutationId", (String) input.get("clientMutationId"));
            result.put("id", newId);
            return result;
        };


        addTodo = todoSchema.getRelay().mutationWithClientMutationId("AddTodo", "addTodo", inputFields, outputFields, mutate);
    }
}