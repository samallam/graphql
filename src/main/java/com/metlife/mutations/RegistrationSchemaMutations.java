/*
 * @(#)com.metlife.mutations.RegistrationSchemaMutations.java
 * ===========================================================================
 * This source code is exclusive propriety of the  Finances. 
 * In no case are the contents allowed to be distributed to third parties or made public 
 * without prior and written consent of Finances.
 * ===========================================================================
 *  
 */
/*
 * Change Activity:
 *
 * Reason    Date       Author         Version     Description
 * ------    ----       ------      -------     -------------------------------------- 
 * created  10-Feb-2016 Amit Mishra       0.1       created the artifact
 * 
 */
package com.metlife.mutations;

import static graphql.Scalars.GraphQLString;
import static graphql.schema.GraphQLFieldDefinition.newFieldDefinition;
import static graphql.schema.GraphQLInputObjectField.newInputObjectField;


import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.metlife.model.Registration;
import com.metlife.restclient.GetRestClient;
import com.metlife.schema.RegistrationSchema;
import com.metlife.utils.JavaToPrettyJSON;
import com.metlife.utils.StringManipulation;

import graphql.relay.Edge;
import graphql.schema.DataFetcher;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLInputObjectField;
import graphql.schema.GraphQLNonNull;

/**
 * <code> RegistrationSchemaMutations <code> class
 * 
 * @version 0.1
 * @author Amit Mishra , IBM Corp.
 *
 */
public class RegistrationSchemaMutations {

	private GraphQLFieldDefinition addTodo;
	private RegistrationSchema todoSchema;

	public RegistrationSchemaMutations(RegistrationSchema todoSchema) {
		this.todoSchema = todoSchema;
		createAddTodoMutation();
	}

	private GraphQLFieldDefinition getViewerField() {
		GraphQLFieldDefinition viewer = newFieldDefinition().name("viewer").type(todoSchema.getUserType())
				.staticValue(todoSchema.getTheOnlyUser()).build();
		return viewer;
	}

	public List<GraphQLFieldDefinition> getFields() {
		return Arrays.asList(addTodo);
	}

	private void createAddTodoMutation() {
		GraphQLInputObjectField textField = newInputObjectField().name("details")
				.type(new GraphQLNonNull(GraphQLString)).build();

		List<GraphQLInputObjectField> inputFields = Arrays.asList(textField);

		GraphQLFieldDefinition todoEdge = newFieldDefinition().name("todoEdge").type(todoSchema.getTodosEdge())
				.dataFetcher(environment -> {
					Map source = (Map) environment.getSource();
					String registrationId = (String) source.get("todoId");
					Registration registration = todoSchema.getRegistration(registrationId);
					return new Edge(registration, todoSchema.getSimpleConnection().cursorForObjectInConnection(registration));
				}).build();

		List<GraphQLFieldDefinition> outputFields = Arrays.asList(todoEdge, getViewerField());

		DataFetcher mutate = environment -> {
			Map<String, Object> input = environment.getArgument("input");
			Map<String, String> formDetails= StringManipulation.getFormDetails(input);
			String firstName = (String) formDetails.get("FirstName");
			String lastName = (String) formDetails.get("LastName");
			String citizenId = (String) formDetails.get("CitizenId");
			String dateOfBirth = (String) formDetails.get("DateOfBirth");
			String postalCode = (String) formDetails.get("PostalCode");
			String emailAddress = (String) formDetails.get("EmailAddress");
			String confirmEmailAddress = (String) formDetails.get("ConfirmEmailAddress");
			String mobilePhone = (String) formDetails.get("MobilePhone");	
			Registration registration = todoSchema.addRegistration(firstName, lastName, citizenId,dateOfBirth,postalCode,
					  emailAddress,  confirmEmailAddress, mobilePhone);
			String newId = registration.getId();
			String JsonObj = JavaToPrettyJSON.JavaToJSON(registration);
			//GetRestClient.getEmployees(JsonObj);
			Map<String, String> result = new LinkedHashMap<>();
			result.put("clientMutationId", (String) input.get("clientMutationId"));
			result.put("todoId", newId);
			return result;
		};

		addTodo = todoSchema.getRelay().mutationWithClientMutationId("AddTodo", "addTodo", inputFields, outputFields,
				mutate);
	}
	

}
