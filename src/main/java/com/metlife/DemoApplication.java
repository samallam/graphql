/*
 * @(#)com.metlife.DemoApplication.java
 * ===========================================================================
 * This source code is exclusive propriety of the  Finances. 
 * In no case are the contents allowed to be distributed to third parties or made public 
 * without prior and written consent of Finances.
 * ===========================================================================
 *  
 */
/*
 * Change Activity:
 *
 * Reason    Date       Author         Version     Description
 * ------    ----       ------      -------     -------------------------------------- 
 * created  01-Feb-2016 Amit Mishra       0.1       created the artifact
 * 
 */
package com.metlife;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.metlife.controller.GraphQLController;

/**
 * <code> DemoApplication <code> class
 * 
 * @version 0.1
 * @author Amit Mishra , IBM Corp.
 *
 */
@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(GraphQLController.class, args);

	}

}
