/*
 * @(#)com.metlife.controller.GraphQLController.java
 * ===========================================================================
 * This source code is exclusive propriety of the  Finances. 
 * In no case are the contents allowed to be distributed to third parties or made public 
 * without prior and written consent of Finances.
 * ===========================================================================
 *  
 */
/*
 * Change Activity:
 *
 * Reason    Date       Author         Version     Description
 * ------    ----       ------      -------     -------------------------------------- 
 * created  10-Feb-2016 Amit Mishra       0.1       created the artifact
 * 
 */
package com.metlife.controller;

import graphql.ExecutionResult;
import graphql.GraphQL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.metlife.schema.AccountSchema;
import com.metlife.schema.RegistrationSchema;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <code> GraphQLController <code> class
 * 
 * @version 0.1
 * @author Amit Mishra , IBM Corp.
 *
 */
@Controller
@EnableAutoConfiguration
public class GraphQLController {

	AccountSchema accountSchema = new AccountSchema();
	RegistrationSchema registrationSchema = new RegistrationSchema();
	GraphQL graphql = null;

	private static final Logger log = LoggerFactory.getLogger(GraphQLController.class);

	@RequestMapping(value = "/graphql", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object executeOperation(@RequestBody Map body) {
		String query = (String) body.get("query");
		System.out.println(query);
		Map<String, Object> variables = (Map<String, Object>) body.get("variables");
		ExecutionResult executionResult = null;
		Map<String, Object> result = new LinkedHashMap<>();
		if (query.contains("policyNumber")) {
			accountSchema.doSchema();
			GraphQL graphql = new GraphQL(accountSchema.getSchema());
			executionResult = graphql.execute(query, (Object) null, variables);
		} else {
			registrationSchema.doRegistration();
			graphql = new GraphQL(registrationSchema.getSchema());
			executionResult = graphql.execute(query, (Object) null, variables);
		}
		if (executionResult.getErrors().size() > 0) {
			result.put("errors", executionResult.getErrors());
			log.error("Errors: {}", executionResult.getErrors());
		}
		result.put("data", executionResult.getData());
		log.info("Sent response to web app " + executionResult.getData());
		return result;
	}

}
