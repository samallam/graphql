/*
 * @(#)com.metlife.restclient.GetRestClient.java
 * ===========================================================================
 * This source code is exclusive propriety of the  Finances. 
 * In no case are the contents allowed to be distributed to third parties or made public 
 * without prior and written consent of Finances.
 * ===========================================================================
 *  
 */
/*
 * Change Activity:
 *
 * Reason    Date       Author         Version     Description
 * ------    ----       ------      -------     -------------------------------------- 
 * created  10-Feb-2016 Amit Mishra       0.1       created the artifact
 * 
 */
package com.metlife.restclient;

import org.springframework.web.client.RestTemplate;

/**
 * <code> GetRestClient <code> class
 * 
 * @version 0.1
 * @author Amit Mishra , IBM Corp.
 *
 */
public class GetRestClient {

	/**
	 * @param jsonObj
	 * @return
	 */
	public static String getEmployees(String jsonObj) {

		final String uri = "http://9.118.90.175:8080/api/metlife/register";
		RestTemplate restTemplate = new RestTemplate();
		String result = restTemplate.postForObject(uri, jsonObj, String.class);
		System.out.println(result);
		return result;

	}

	/**
	 * @param jsonObj
	 * @return
	 */
	public static String getAccount(String jsonObj) {

		final String uri = "http://9.118.90.175:8080/api/metlife/account";
		RestTemplate restTemplate = new RestTemplate();
		String result = restTemplate.postForObject(uri, jsonObj, String.class);
		System.out.println(result);
		return result;

	}

}
