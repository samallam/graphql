/*
 * @(#)com.metlife.schema.RegistrationSchema.java
 * ===========================================================================
 * This source code is exclusive propriety of the  Finances. 
 * In no case are the contents allowed to be distributed to third parties or made public 
 * without prior and written consent of Finances.
 * ===========================================================================
 *  
 */
/*
 * Change Activity:
 *
 * Reason    Date       Author         Version     Description
 * ------    ----       ------      -------     -------------------------------------- 
 * created  01-Feb-2016 Amit Mishra       0.1       created the artifact
 * 
 */
package com.metlife.schema;

import graphql.Scalars;
import graphql.relay.Connection;
import graphql.relay.Relay;
import graphql.relay.SimpleListConnection;
import graphql.schema.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.metlife.model.Registration;
import com.metlife.model.User;
import com.metlife.mutations.RegistrationSchemaMutations;

import static graphql.Scalars.GraphQLID;
import static graphql.Scalars.GraphQLInt;
import static graphql.schema.GraphQLFieldDefinition.newFieldDefinition;
import static graphql.schema.GraphQLObjectType.newObject;
import static graphql.schema.GraphQLSchema.newSchema;

/**
 * <code> RegistrationSchema <code> class
 * 
 * @version 0.1
 * @author Amit Mishra , IBM Corp.
 *
 */

public class RegistrationSchema {

	private GraphQLSchema schema;

	private GraphQLObjectType registrationType;

	private GraphQLObjectType userType;

	private GraphQLObjectType connectionFromUserToTodos;

	private GraphQLInterfaceType nodeInterface;

	private GraphQLObjectType todosEdge;

	private User theOnlyUser = new User();

	private List<Registration> registrations = new ArrayList<>();

	private SimpleListConnection simpleConnection;

	private Relay relay = new Relay();

	int nextRegistrationId = 0;

	public RegistrationSchema() {
		addRegistration("IBM", "Corp", "012345","IBM", "Corp", "012345","IBM", "Corp");
		addRegistration("IBM1", "Corp1", "0123451","IBM1", "Corp1", "0123451","IBM1", "Corp1");
		//createSchema();
	}
	
	public void  doRegistration() {
		createSchema();
	}

	private void createSchema() {
		TypeResolverProxy typeResolverProxy = new TypeResolverProxy();
		nodeInterface = relay.nodeInterface(typeResolverProxy);
		simpleConnection = new SimpleListConnection(registrations);

		createRegistrationType();
		createConnectionFromUserToTodos();
		createUserType();

		typeResolverProxy.setTypeResolver(object -> {
			if (object instanceof User) {
				return userType;
			} else if (object instanceof Registration) {
				return registrationType;
			}
			return null;
		});

		DataFetcher todoDataFetcher = environment -> {
			String id = environment.getArgument("id");
			return new User();
		};

		GraphQLObjectType QueryRoot = newObject().name("Root")
				.field(newFieldDefinition().name("viewer").type(userType).staticValue(theOnlyUser).build())
				.field(relay.nodeField(nodeInterface, todoDataFetcher)).build();

		RegistrationSchemaMutations todoSchemaMutations = new RegistrationSchemaMutations(this);

		GraphQLObjectType mutationType = newObject().name("Mutation").fields(todoSchemaMutations.getFields()).build();

		schema = newSchema().query(QueryRoot).mutation(mutationType).build();
		System.out.println(schema);
				
	}

	private void createUserType() {
		userType = newObject().name("User")
				.field(newFieldDefinition().name("id").type(new GraphQLNonNull(GraphQLID)).dataFetcher(environment -> {
					User user = (User) environment.getSource();
					return relay.toGlobalId("User", user.getId());
				}).build())
				.field(newFieldDefinition().name("todos").type(connectionFromUserToTodos)
						.argument(relay.getConnectionFieldArguments()).dataFetcher(simpleConnection).build())
				.withInterface(nodeInterface).build();
	}

	private void createRegistrationType() {
		registrationType = newObject().name("Registration")
				.field(newFieldDefinition().name("id").type(new GraphQLNonNull(GraphQLID)).dataFetcher(environment -> {
					Registration registration = (Registration) environment.getSource();
					return relay.toGlobalId("Registration", registration.getId());
				}).build()).field(newFieldDefinition().name("firstName").type(Scalars.GraphQLString).build())
				.field(newFieldDefinition().name("lastName").type(Scalars.GraphQLString).build())
				.field(newFieldDefinition().name("citizenId").type(Scalars.GraphQLString).build())
				.field(newFieldDefinition().name("dateOfBirth").type(Scalars.GraphQLString).build())
				.field(newFieldDefinition().name("postalCode").type(Scalars.GraphQLString).build())
				.field(newFieldDefinition().name("emailAddress").type(Scalars.GraphQLString).build())
				.field(newFieldDefinition().name("confirmEmailAddress").type(Scalars.GraphQLString).build())
				.field(newFieldDefinition().name("mobilePhone").type(Scalars.GraphQLString).build())
				.field(newFieldDefinition().name("complete").type(Scalars.GraphQLBoolean).build())
				.withInterface(nodeInterface).build();
	}

	private void createConnectionFromUserToTodos() {
		todosEdge = relay.edgeType("Todo", registrationType, nodeInterface,
				Collections.<GraphQLFieldDefinition> emptyList());
		GraphQLFieldDefinition totalCount = newFieldDefinition().name("totalCount").type(GraphQLInt)
				.dataFetcher(environment -> {
					Connection connection = (Connection) environment.getSource();
					return connection.getEdges().size();
				}).build();

		GraphQLFieldDefinition completedCount = newFieldDefinition().name("completedCount").type(GraphQLInt)
				.dataFetcher(environment -> {
					Connection connection = (Connection) environment.getSource();
					return (int) connection.getEdges().stream()
							.filter(edge -> ((Registration) edge.getNode()).isComplete()).count();
				}).build();
		connectionFromUserToTodos = relay.connectionType("Todo", todosEdge, Arrays.asList(totalCount, completedCount));
	}

	public Registration addRegistration(String firstName, String lastName, String citizenId,String dateOfBirth,String postalCode,
								 String emailAddress, String confirmEmailAddress,String mobilePhone) {
		Registration newRegistration = new Registration();
		newRegistration.setId(Integer.toString(nextRegistrationId++));
		newRegistration.setFirstName(firstName);
		newRegistration.setLastName(lastName);
		newRegistration.setCitizenId(citizenId);
		newRegistration.setDateOfBirth(dateOfBirth);
		newRegistration.setPostalCode(postalCode);
		newRegistration.setEmailAddress(emailAddress);
		newRegistration.setConfirmEmailAddress(confirmEmailAddress);
		newRegistration.setMobilePhone(mobilePhone);
		registrations.add(newRegistration);
		return newRegistration;
	}

	public Registration getRegistration(String id) {
		return registrations.stream().filter(registration -> registration.getId().equals(id)).findFirst().get();
	}

	public List<Registration> getRegistrations(List<String> ids) {
		return registrations.stream().filter(registration -> ids.contains(registration.getId()))
				.collect(Collectors.toList());
	}

	public GraphQLSchema getSchema() {
		return schema;
	}

	public User getTheOnlyUser() {
		return theOnlyUser;
	}

	public SimpleListConnection getSimpleConnection() {
		return simpleConnection;
	}

	public GraphQLObjectType getUserType() {
		return userType;
	}

	public GraphQLObjectType getRegistrationType() {
		return registrationType;
	}

	public GraphQLObjectType getTodosEdge() {
		return todosEdge;
	}

	public Relay getRelay() {
		return relay;
	}
}
