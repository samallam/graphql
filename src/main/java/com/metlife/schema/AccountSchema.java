/*
 * @(#)com.metlife.schema.AccountSchema.java
 * ===========================================================================
 * This source code is exclusive propriety of the  Finances. 
 * In no case are the contents allowed to be distributed to third parties or made public 
 * without prior and written consent of Finances.
 * ===========================================================================
 *  
 */
/*
 * Change Activity:
 *
 * Reason    Date       Author         Version     Description
 * ------    ----       ------      -------     -------------------------------------- 
 * created  10-Feb-2016 Amit Mishra       0.1       created the artifact
 * 
 */
package com.metlife.schema;

import graphql.relay.Relay;
import graphql.relay.SimpleListConnection;
import graphql.schema.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.metlife.model.Account;
import com.metlife.model.User;
import com.metlife.restclient.GetRestClient;
import com.metlife.utils.JavaToPrettyJSON;

import static graphql.Scalars.GraphQLID;
import static graphql.Scalars.GraphQLString;
import static graphql.schema.GraphQLFieldDefinition.newFieldDefinition;
import static graphql.schema.GraphQLObjectType.newObject;
import static graphql.schema.GraphQLSchema.newSchema;


/**
 * <code> AccountSchema <code> class
 * 
 * @version 0.1
 * @author Amit Mishra , IBM Corp.
 *
 */
public class AccountSchema {


    private GraphQLSchema schema;
   
    private GraphQLObjectType userType;
    private GraphQLObjectType accountsEdge;
    private User theOnlyUser = new User();
    private List<Account> accounts = null;
    private SimpleListConnection simpleConnection;
    private Relay relay = new Relay();
    private GraphQLObjectType connectionFromUserToAccounts;
    private GraphQLInterfaceType nodeInterface;
	private GraphQLObjectType accountType;
	private GraphQLObjectType insuranceInfoType;
	private GraphQLObjectType metlifeGroupBenefitsType;
	private GraphQLObjectType diabilityIncomeInsuraceType;
	private GraphQLObjectType annuitiesInfoType;
	private GraphQLObjectType annuitiesInfoMliType;
    int nextTodoId = 0;


    public AccountSchema() {
    }
    
    public  void doSchema() {
    	accounts = new ArrayList<>();
        User user = new User();
        user.setId("12345-1233");
        
       	//start REST
        String JsonObj = JavaToPrettyJSON.JavaToJSON(user.getId());
    	accounts.add( JavaToPrettyJSON.JsonToJava(GetRestClient.getAccount(JsonObj)));
    	Account  account = new Account();
        //END  REST
         
        addAccount(account,accounts.get(0));
        createSchema();	
    }


    private void createSchema() {
        TypeResolverProxy typeResolverProxy = new TypeResolverProxy();
        nodeInterface = relay.nodeInterface(typeResolverProxy);
        simpleConnection = new SimpleListConnection(accounts);
        //createInsuranceInfoType(accounts.get(0));.
        insuranceInfoType = AccountSchemaHelper.createInsuranceType(nodeInterface, relay, accounts.get(0));
        metlifeGroupBenefitsType = AccountSchemaHelper.createMetlifeGroupBenefitsType(nodeInterface, relay, accounts.get(0));
        diabilityIncomeInsuraceType = AccountSchemaHelper.createDiabilityIncomeInsuraceType(nodeInterface, relay, accounts.get(0));
        annuitiesInfoType = AccountSchemaHelper.createAnnuitiesInfoType(nodeInterface, relay, accounts.get(0));
        annuitiesInfoMliType = AccountSchemaHelper.createAnnuitiesInfoMliType(nodeInterface, relay, accounts.get(0));
        createAccountType(accounts.get(0));
        createConnectionFromUserToAccounts();
        createUserType();

        typeResolverProxy.setTypeResolver(object -> {
            if (object instanceof User) {
                return userType;
            } else if (object instanceof Account) {
                return accountType;
            }
            return null;
        });

        DataFetcher todoDataFetcher = environment -> {
            String id = environment.getArgument("id");
            return new User();
        };

        GraphQLObjectType QueryRoot = newObject()
                .name("Root")
                .field(newFieldDefinition()
                        .name("viewer")
                        .type(userType)
                        .staticValue(theOnlyUser)
                        .build())
                .field(relay.nodeField(nodeInterface, todoDataFetcher))
                .build();

         schema = newSchema().query(QueryRoot).build();
    }

    private void createUserType() {
        userType = newObject()
                .name("User")
                .field(newFieldDefinition()
                        .name("id")
                        .type(new GraphQLNonNull(GraphQLID))
                        .dataFetcher(environment -> {
                                    User user = (User) environment.getSource();
                                    return relay.toGlobalId("User", user.getId());
                                }
                        )
                        .build())
                .field(newFieldDefinition()
                        .name("accounts")
                        .type(connectionFromUserToAccounts)
                        	.argument(relay.getConnectionFieldArguments()).dataFetcher(simpleConnection).build())
                        .withInterface(nodeInterface)
                        .build();
    }

    private void createConnectionFromUserToAccounts() {
    	accountsEdge = relay.edgeType("Account", accountType, nodeInterface, Collections.<GraphQLFieldDefinition>emptyList());
        GraphQLFieldDefinition policyNumber = newFieldDefinition()
                .name("policyNumber")
                .staticValue("12345678")
                .type(GraphQLString)
                .build();

        GraphQLFieldDefinition accountNumber = newFieldDefinition()
                .name("accountNumber")
                .type(GraphQLString)
                .staticValue("9876543")
                .build();
        connectionFromUserToAccounts = relay.connectionType("Account", accountsEdge, Arrays.asList(policyNumber, accountNumber));
    }
    
    public  void createAccountType(Account  acct) {
		accountType   = newObject()
           .name("Account")
           .description("Account.")
           .field(newFieldDefinition()
                   .name("id")
                   .type(new GraphQLNonNull(GraphQLID))
                   .dataFetcher(environment -> {
                               Account account = (Account) environment.getSource();
                               return relay.toGlobalId("Account", account.getId());
                           }
                   )
                   .build())
           .field(newFieldDefinition()
                   .name("policyNumber")
                   .description("policyNumber")
                   .staticValue(acct.getPolicyNumber())
                   .type(new GraphQLNonNull(GraphQLString))
                   .build())
           .field(newFieldDefinition()
                   .name("accountNumber")
                   .description("accountNumber")
                   .type(GraphQLString)
                   .staticValue(acct.getAccountNumber())
                   .build())
           .field(newFieldDefinition()
                   .name("citizenId")
                   .description("accountNumber")
                   .type(GraphQLString)
                   .staticValue(acct.getCitizenId())
                   .build())
           .field(newFieldDefinition()
                   .name("insuranceInfo")
                   .description("insuranceInfo")
                   .type(insuranceInfoType)
                   .staticValue(acct.getInsuranceInfo())
                   .build())
           .field(newFieldDefinition()
                   .name("metlifeGroupBenefitsInfo")
                   .description("MetlifeGroupBenefitsInfo")
                   .type(metlifeGroupBenefitsType)
                   .staticValue(acct.getMetlifeGroupBenefitsInfo())
                   .build())
           .field(newFieldDefinition()
                   .name("diabilityIncomeInsuraceInfo")
                   .description("DiabilityIncomeInsuraceInfo")
                   .type(diabilityIncomeInsuraceType)
                   .staticValue(acct.getDiabilityIncomeInsuraceInfo())
                   .build())
           .field(newFieldDefinition()
                   .name("annuitiesInfo")
                   .description("AnnuitiesInfo")
                   .type(annuitiesInfoType)
                   .staticValue(acct.getAnnuitiesInfo())
                   .build())
           .field(newFieldDefinition()
                   .name("annuitiesInfoMli")
                   .description("AnnuitiesInfoMli")
                   .type(annuitiesInfoMliType)
                   .staticValue(acct.getAnnuitiesInfoMli())
                   .build())
           .withInterface(nodeInterface)
           .build();
	}
    
   
    public User getTheOnlyUser() {
        return theOnlyUser;
    }


    public SimpleListConnection getSimpleConnection() {
        return simpleConnection;
    }


    public GraphQLObjectType getUserType() {
        return userType;
    }


    public GraphQLObjectType getAccountsEdge() {
        return accountsEdge;
    }


    public Relay getRelay() {
        return relay;
    }


    public String addAccount(Account inobj,Account obj ) {
    	obj.setId(Integer.toString(nextTodoId++));
    	/*obj.setAccountNumber(inobj.getAccountNumber());
    	obj.setPolicyNumber(inobj.getPolicyNumber());
    	obj.setInsuranceInfo(inobj.getInsuranceInfo());*/
         return obj.getId();
    }

    public Account getAccount(String id) {
        return accounts.stream().filter(account -> account.getId().equals(id)).findFirst().get();
    }

    public List<Account> getAccounts(List<String> ids) {
        return accounts.stream().filter(account -> ids.contains(account.getId())).collect(Collectors.toList());
    }

    public GraphQLSchema getSchema() {
        return schema;
    }
    /**
	 * @return the accountType
	 */
	public GraphQLObjectType getAccountType() {
		return accountType;
	}

}
