/*
 * @(#)com.metlife.schema.AccountSchemaHelper.java
 * ===========================================================================
 * This source code is exclusive propriety of the  Finances. 
 * In no case are the contents allowed to be distributed to third parties or made public 
 * without prior and written consent of Finances.
 * ===========================================================================
 *  
 */
/*
 * Change Activity:
 *
 * Reason    Date       Author         Version     Description
 * ------    ----       ------      -------     -------------------------------------- 
 * created  10-Feb-2016 Amit Mishra       0.1       created the artifact
 * 
 */
package com.metlife.schema;

import static graphql.Scalars.GraphQLID;
import static graphql.Scalars.GraphQLString;
import static graphql.schema.GraphQLFieldDefinition.newFieldDefinition;
import static graphql.schema.GraphQLObjectType.newObject;
import com.metlife.model.Account;
import com.metlife.model.AnnuitiesInfo;
import com.metlife.model.AnnuitiesInfoMli;
import com.metlife.model.DiabilityIncomeInsurace;
import com.metlife.model.InsuranceInfo;
import com.metlife.model.MetlifeGroupBenefits;

import graphql.relay.Relay;
import graphql.schema.GraphQLInterfaceType;
import graphql.schema.GraphQLNonNull;
import graphql.schema.GraphQLObjectType;



/**
 * <code> AccountSchemaHelper <code> class
 * 
 * @version 0.1
 * @author Amit Mishra , IBM Corp.
 *
 */
public class AccountSchemaHelper {
	
	private static GraphQLObjectType annuitiesInfoType;
	private static GraphQLObjectType annuitiesInfoMliType;
	private static GraphQLObjectType diabilityIncomeInsuraceType;
	private static GraphQLObjectType metlifeGroupBenefitsType;
	private static GraphQLObjectType insuranceType;
	
   /**
 * @param nodeInterface
 * @param relay
 * @param account
 * @param annuitiesInfoType
 */
	
public static  GraphQLObjectType createAnnuitiesInfoMliType(GraphQLInterfaceType nodeInterface,Relay relay,Account  account) {
		annuitiesInfoMliType   = newObject()
	      .name("AnnuitiesInfoMli")
	      .description("first Annuities2")
	      .field(newFieldDefinition()
	              .name("id")
	              .type(new GraphQLNonNull(GraphQLID))
	              .dataFetcher(environment -> {
	            	  	AnnuitiesInfoMli  annutiesmli = (AnnuitiesInfoMli) environment.getSource();
	                          return relay.toGlobalId("AnnuitiesInfoMli", annutiesmli.getId());
	                      }
	              )
	              .build())
	      .field(newFieldDefinition()
	              .name("flexiblePremiumLife")
	              .description("Flexible Premium life.")
	              .type(new GraphQLNonNull(GraphQLString))
	              .staticValue(account.getAnnuitiesInfoMli().getFlexiblePremiumLife())
	              .build())
	      .field(newFieldDefinition()
	              .name("accountValue")
	              .description("accountValue")
	              .type(GraphQLString)
	              .staticValue(account.getAnnuitiesInfoMli().getAccountValue())
	              .build())
	      .field(newFieldDefinition()
	              .name("accountValueDate")
	              .description("accountValueDate")
	              .type(GraphQLString)
	              .staticValue(account.getAnnuitiesInfoMli().getAccountValueDate())
	              .build())
	      .field(newFieldDefinition()
	              .name("insured")
	              .description("insured")
	              .type(GraphQLString)
	               .staticValue(account.getAnnuitiesInfoMli().getInsured())
	              .build())
	      .field(newFieldDefinition()
	              .name("status")
	              .description("status")
	              .type(GraphQLString)
	              .staticValue(account.getAnnuitiesInfoMli().getStatus())
	              .build())
	      .field(newFieldDefinition()
	              .name("priBeneficiaries")
	              .description("priBeneficiaries")
	              .type(GraphQLString)
	              .staticValue(account.getAnnuitiesInfoMli().getPriBeneficiaries())
	              .build())
	      .build();
		 return annuitiesInfoMliType;
	}
	
	public static  GraphQLObjectType createAnnuitiesInfoType(GraphQLInterfaceType nodeInterface,Relay relay,Account  account) {
		 annuitiesInfoType   = newObject()
           .name("AnnuitiesInfo")
           .description("first Annuities1")
           .field(newFieldDefinition()
                   .name("id")
                   .type(new GraphQLNonNull(GraphQLID))
                   .dataFetcher(environment -> {
                	   		   AnnuitiesInfo  annuties = (AnnuitiesInfo) environment.getSource();
                               return relay.toGlobalId("AnnuitiesInfo", annuties.getId());
                           }
                   )
                   .build())
           .field(newFieldDefinition()
                   .name("flexiblePremiumLife")
                   .description("Flexible Premium life.")
                   .type(new GraphQLNonNull(GraphQLString))
                   .staticValue(account.getAnnuitiesInfo().getFlexiblePremiumLife())
                   .build())
           .field(newFieldDefinition()
                   .name("accountValue")
                   .description("accountValue")
                   .type(GraphQLString)
                   .staticValue(account.getAnnuitiesInfo().getAccountValue())
                   .build())
           .field(newFieldDefinition()
                   .name("accountValueDate")
                   .description("accountValueDate")
                   .type(GraphQLString)
                   .staticValue(account.getAnnuitiesInfo().getAccountValueDate())
                   .build())
           .field(newFieldDefinition()
                   .name("insured")
                   .description("insured")
                   .type(GraphQLString)
                    .staticValue(account.getAnnuitiesInfo().getInsured())
                   .build())
           .field(newFieldDefinition()
                   .name("status")
                   .description("status")
                   .type(GraphQLString)
                   .staticValue(account.getAnnuitiesInfo().getStatus())
                   .build())
           .field(newFieldDefinition()
                   .name("priBeneficiaries")
                   .description("priBeneficiaries")
                   .type(GraphQLString)
                   .staticValue(account.getAnnuitiesInfo().getPriBeneficiaries())
                   .build())
           .build();
		 return annuitiesInfoType;
	}

	 public  static  GraphQLObjectType  createDiabilityIncomeInsuraceType(GraphQLInterfaceType nodeInterface,Relay relay,Account  account) {
		diabilityIncomeInsuraceType   = newObject()
           .name("DiabilityIncomeInsuraceInfo")
           .description("DiabilityIncomeInsurace Premium life.")
           .field(newFieldDefinition()
                   .name("id")
                   .type(new GraphQLNonNull(GraphQLID))
                   .dataFetcher(environment -> {
                	   		   DiabilityIncomeInsurace diabilityInfo = (DiabilityIncomeInsurace) environment.getSource();
                               return relay.toGlobalId("DiabilityIncomeInsurace", diabilityInfo.getId());
                           }
                   )
                   .build())
           .field(newFieldDefinition()
                   .name("omniAdvantage")
                   .description("OmniAdvantage.")
                   .type(new GraphQLNonNull(GraphQLString))
                   .staticValue(account.getDiabilityIncomeInsuraceInfo().getOmniAdvantage())
                   .build())
           .field(newFieldDefinition()
                   .name("monthlyBenefit")
                   .description("monthlyBenefit")
                   .type(GraphQLString)
                   .staticValue(account.getDiabilityIncomeInsuraceInfo().getMonthlyBenefit())
                   .build())
           .field(newFieldDefinition()
                   .name("monthlyBenefitDate")
                   .description("monthlyBenefitDate")
                   .type(GraphQLString)
                   .staticValue(account.getDiabilityIncomeInsuraceInfo().getMonthlyBenefitDate())
                   .build())
           .field(newFieldDefinition()
                   .name("lumpSum")
                   .description("LumpSum")
                   .type(GraphQLString)
                   .staticValue(account.getDiabilityIncomeInsuraceInfo().getLumpSum())
                   .build())
           .field(newFieldDefinition()
                   .name("lumpSumDate")
                   .description("LumpSumDate")
                   .type(GraphQLString)
                   .staticValue(account.getDiabilityIncomeInsuraceInfo().getPrePayDue())
                   .build())
           .field(newFieldDefinition()
                   .name("prePayDue")
                   .description("prePayDue")
                   .type(GraphQLString)
                   .staticValue(account.getDiabilityIncomeInsuraceInfo().getPrePayDue())
                   .build())
           .field(newFieldDefinition()
                   .name("prePayDueDate")
                   .description("prePayDueDate")
                   .type(GraphQLString)
                   .staticValue(account.getDiabilityIncomeInsuraceInfo().getPrePayDueDate())
                   .build())
           .field(newFieldDefinition()
                   .name("billingOption")
                   .description("billingOption")
                   .type(GraphQLString)
                   .staticValue(account.getDiabilityIncomeInsuraceInfo().getBillingOption())
                   .build())
           .field(newFieldDefinition()
                   .name("insured")
                   .description("insured")
                   .type(GraphQLString)
                   .staticValue(account.getDiabilityIncomeInsuraceInfo().getInsured())
                   .build())
           .field(newFieldDefinition()
                   .name("status")
                   .description("status")
                   .staticValue(account.getDiabilityIncomeInsuraceInfo().getStatus())
                   .type(GraphQLString)
                   .build())
           .field(newFieldDefinition()
                   .name("priBeneficiaries")
                   .description("priBeneficiaries")
                   .type(GraphQLString)
                   .staticValue(account.getDiabilityIncomeInsuraceInfo().getPriBeneficiaries())
                   .build())
           .build();
		return diabilityIncomeInsuraceType;
	 }
   
	 public  static  GraphQLObjectType  createMetlifeGroupBenefitsType(GraphQLInterfaceType nodeInterface,Relay relay,Account  account) {
		metlifeGroupBenefitsType   = newObject()
           .name("MetlifeGroupBenefitsInfo")
           .description("guar_level_term.")
           .field(newFieldDefinition()
                   .name("id")
                   .type(new GraphQLNonNull(GraphQLID))
                   .dataFetcher(environment -> {
                	   		   MetlifeGroupBenefits benifit = (MetlifeGroupBenefits) environment.getSource();
                               return relay.toGlobalId("MetlifeGroupBenefits", benifit.getId());
                           }
                   )
                   .build())
           .field(newFieldDefinition()
                   .name("guarLevelTerm")
                   .description("guar_level_term.")
                   .type(new GraphQLNonNull(GraphQLString))
                   .staticValue(account.getMetlifeGroupBenefitsInfo().getGuarLevelTerm())
                   .build())
           .field(newFieldDefinition()
                   .name("faceAmount")
                   .description("face_amount.")
                   .type(new GraphQLNonNull(GraphQLString))
                   .staticValue(account.getMetlifeGroupBenefitsInfo().getFaceAmount())
                   .build())
           .field(newFieldDefinition()
                   .name("faceAmountDate")
                   .description("faceAmountDate")
                   .type(new GraphQLNonNull(GraphQLString))
                   .staticValue(account.getMetlifeGroupBenefitsInfo().getFaceAmountDate())
                   .build())
           .field(newFieldDefinition()
                   .name("deathBenefit")
                   .description("deathBenefit")
                   .staticValue(account.getMetlifeGroupBenefitsInfo().getDeathBenefit())
                   .type(GraphQLString)
                   .build())
           .field(newFieldDefinition()
                   .name("deathBenefitDate")
                   .description("deathBenefitDate")
                   .staticValue(account.getMetlifeGroupBenefitsInfo().getDeathBenefitDate())
                   .type(GraphQLString)
                   .build())
           .field(newFieldDefinition()
                   .name("prePayDue")
                   .description("prePayDue")
                   .staticValue(account.getMetlifeGroupBenefitsInfo().getPrePayDue())
                   .type(GraphQLString)
                   .build())
           .field(newFieldDefinition()
                   .name("prePayDueDate")
                   .description("prePayDueDate")
                   .staticValue(account.getMetlifeGroupBenefitsInfo().getPrePayDueDate())
                   .type(GraphQLString)
                   .build())
           .field(newFieldDefinition()
                   .name("billingOption")
                   .description("billingOption")
                   .staticValue(account.getMetlifeGroupBenefitsInfo().getBillingOption())
                   .type(GraphQLString)
                   .build())
           .field(newFieldDefinition()
                   .name("insured")
                   .description("insured")
                   .type(GraphQLString)
                   .staticValue(account.getMetlifeGroupBenefitsInfo().getInsured())
                   .build())
           .field(newFieldDefinition()
                   .name("status")
                   .description("status")
                   .staticValue(account.getMetlifeGroupBenefitsInfo().getStatus())
                   .type(GraphQLString)
                   .build())
           .field(newFieldDefinition()
                   .name("priBeneficiaries")
                   .description("priBeneficiaries")
                   .type(GraphQLString)
                   .staticValue(account.getMetlifeGroupBenefitsInfo().getPriBeneficiaries())
                   .build())
           .build();
		return metlifeGroupBenefitsType;
	}
	
	 public   static  GraphQLObjectType  createInsuranceType(GraphQLInterfaceType nodeInterface,Relay relay,Account  account) {    
        insuranceType   = newObject()
           .name("InsuranceInfo")
           .description("Insurance")
           .field(newFieldDefinition()
                   .name("id")
                   .type(new GraphQLNonNull(GraphQLID))
                   .dataFetcher(environment -> {
                	   		   InsuranceInfo insurance = (InsuranceInfo) environment.getSource();
                               return relay.toGlobalId("InsuranceInfo", insurance.getId());
                           }
                   )
                   .build())
           .field(newFieldDefinition()
                   .name("flexiblePremiumLife")
                   .description("flexible_premium_life.")
                   .type(new GraphQLNonNull(GraphQLString))
                   .staticValue(account.getInsuranceInfo().getFlexiblePremiumLife())
                   .build())
           .field(newFieldDefinition()
                   .name("faceAmount")
                   .description("face_amount")
                   .staticValue(account.getInsuranceInfo().getFaceAmount())
                   .type(GraphQLString)
                   .build())
           .field(newFieldDefinition()
                   .name("faceAmountDate")
                   .description("faceAmountDate")
                   .staticValue(account.getInsuranceInfo().getFaceAmountDate())
                   .type(GraphQLString)
                   .build())
           .field(newFieldDefinition()
                   .name("deathBenefit")
                   .description("death_benefit")
                   .staticValue(account.getInsuranceInfo().getDeathBenefit())
                   .type(GraphQLString)
                   .build())
           .field(newFieldDefinition()
                   .name("deathBenefitDate")
                   .description("deathBenefitDate;")
                   .staticValue(account.getInsuranceInfo().getDeathBenefitDate())
                   .type(GraphQLString)
                   .build())
           .field(newFieldDefinition()
                   .name("prePayDue")
                   .description("prePayDue")
                   .type(GraphQLString)
                   .staticValue(account.getInsuranceInfo().getPrePayDue())
                   .build())
           .field(newFieldDefinition()
                   .name("prePayDueDate")
                   .description("prePayDueDate")
                   .type(GraphQLString)
                   .staticValue(account.getInsuranceInfo().getPrePayDueDate())
                   .build())
           .field(newFieldDefinition()
                   .name("billingOption")
                   .description("billingOption")
                   .type(GraphQLString)
                   .staticValue(account.getInsuranceInfo().getBillingOption())
                   .build())
           .field(newFieldDefinition()
                   .name("insured")
                   .description("insured")
                   .type(GraphQLString)
                   .staticValue(account.getInsuranceInfo().getInsured())
                   .build())
           .field(newFieldDefinition()
                   .name("status")
                   .description("status")
                   .staticValue(account.getInsuranceInfo().getStatus())
                   .type(GraphQLString)
                   .build())
           .field(newFieldDefinition()
                   .name("priBeneficiaries")
                   .description("priBeneficiaries")
                   .type(GraphQLString)
                   .staticValue(account.getInsuranceInfo().getPriBeneficiaries())
                   .build())
           .build();
        return insuranceType;
	}
   

}
