/*
 * @(#)com.metlife.utils.JavaToJSON.java
 * ===========================================================================
 * This source code is exclusive propriety of the  Finances. 
 * In no case are the contents allowed to be distributed to third parties or made public 
 * without prior and written consent of Finances.
 * ===========================================================================
 *  
 */
/*
 * Change Activity:
 *
 * Reason    Date       Author         Version     Description
 * ------    ----       ------      -------     -------------------------------------- 
 * created  10-Feb-2016 Amit Mishra       0.1       created the artifact
 * 
 */
package com.metlife.utils;

import java.io.IOException;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.metlife.model.Account;

/**
 * <code> JavaToJSON <code> class
 * 
 * @version 0.1
 * @author Amit Mishra , IBM Corp.
 *
 */
public class JavaToPrettyJSON {

	/**
	 * @param obj
	 * @return
	 */
	public static String JavaToJSON(Object obj) {
		String jsonInString = "";
		try {
			// Object to JSON in String
			ObjectMapper mapper = new ObjectMapper();
			jsonInString = mapper.writeValueAsString(obj);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonInString;
	}

	/**
	 * @param jsonInString
	 * @return
	 */
	public static Account JsonToJava(String jsonInString) {
		Account account = new Account();
		try {
			// Object to JSON in String
			ObjectMapper mapper = new ObjectMapper();
			// JSON from String to Object
			account = mapper.readValue(jsonInString, Account.class);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return account;
	}
}
