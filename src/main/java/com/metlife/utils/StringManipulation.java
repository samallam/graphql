/*
 * @(#)com.metlife.utils.StringManipulation.java
 * ===========================================================================
 * This source code is exclusive propriety of the  Finances. 
 * In no case are the contents allowed to be distributed to third parties or made public 
 * without prior and written consent of Finances.
 * ===========================================================================
 *  
 */
/*
 * Change Activity:
 *
 * Reason    Date       Author         Version     Description
 * ------    ----       ------      -------     -------------------------------------- 
 * created  10-Feb-2016 Amit Mishra       0.1       created the artifact
 * 
 */
package com.metlife.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * <code> StringManipulation <code> class
 * 
 * @version 0.1
 * @author Amit Mishra , IBM Corp.
 *
 */
public class StringManipulation {

	/**
	 * @param input
	 * @return
	 */
	public static Map<String, String> getFormDetails(Map<String, Object> input) {
		String details = (String) input.get("details");
		details = details.replace('{', ' ');
		details = details.replace('}', ' ');
		String[] arrDetails = details.split(",");
		Map<String, String> formData = new HashMap<>();
		for (int i = 0; i < arrDetails.length; i++) {
			String[] temp = arrDetails[i].split("=");
			formData.put(temp[0].trim(), temp[1].trim());
		}
		return formData;
	}

}
