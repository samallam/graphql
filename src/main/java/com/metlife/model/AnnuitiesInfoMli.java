/*
 * @(#) com.metlife.model.AnnuitiesInfo.java
 * ===========================================================================
 * This source code is exclusive propriety of the  Finances. 
 * In no case are the contents allowed to be distributed to third parties or made public 
 * without prior and written consent of Finances.
 * ===========================================================================
 *  
 */
/*
 * Change Activity:
 *
 * Reason    Date       Author         Version     Description
 * ------    ----       ------      -------     -------------------------------------- 
 * created  10-Feb-2016 Amit Mishra       0.1       created the artifact
 * 
 */
package com.metlife.model;

/**
 * <code> AnnuitiesInfoMli <code> class
 * 
 * @version 0.1
 * @author Amit Mishra , IBM Corp.
 *
 */
public class AnnuitiesInfoMli {
	private String id;
	private String flexiblePremiumLife;
	private String accountValue;
	private String accountValueDate;
	private String insured;
	private String status;
	private String priBeneficiaries;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the accountValue
	 */
	public String getAccountValue() {
		return accountValue;
	}

	/**
	 * @param accountValue
	 *            the accountValue to set
	 */
	public void setAccountValue(String accountValue) {
		this.accountValue = accountValue;
	}

	/**
	 * @return the insured
	 */
	public String getInsured() {
		return insured;
	}

	/**
	 * @param insured
	 *            the insured to set
	 */
	public void setInsured(String insured) {
		this.insured = insured;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the priBeneficiaries
	 */
	public String getPriBeneficiaries() {
		return priBeneficiaries;
	}

	/**
	 * @param priBeneficiaries
	 *            the priBeneficiaries to set
	 */
	public void setPriBeneficiaries(String priBeneficiaries) {
		this.priBeneficiaries = priBeneficiaries;
	}

	/**
	 * @return the flexiblePremiumLife
	 */
	public String getFlexiblePremiumLife() {
		return flexiblePremiumLife;
	}

	/**
	 * @param flexiblePremiumLife
	 *            the flexiblePremiumLife to set
	 */
	public void setFlexiblePremiumLife(String flexiblePremiumLife) {
		this.flexiblePremiumLife = flexiblePremiumLife;
	}

	/**
	 * @return the accountValueDate
	 */
	public String getAccountValueDate() {
		return accountValueDate;
	}

	/**
	 * @param accountValueDate
	 *            the accountValueDate to set
	 */
	public void setAccountValueDate(String accountValueDate) {
		this.accountValueDate = accountValueDate;
	}

}
