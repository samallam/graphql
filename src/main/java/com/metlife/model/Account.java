/*
 * @(#) com.metlife.model.Account.java
 * ===========================================================================
 * This source code is exclusive propriety of the  Finances. 
 * In no case are the contents allowed to be distributed to third parties or made public 
 * without prior and written consent of Finances.
 * ===========================================================================
 *  
 */
/*
 * Change Activity:
 *
 * Reason    Date       Author         Version     Description
 * ------    ----       ------      -------     -------------------------------------- 
 * created  10-Feb-2016 Amit Mishra       0.1       created the artifact
 * 
 */
package com.metlife.model;

/**
 * <code> Account <code> class
 * 
 * @version 0.1
 * @author Amit Mishra , IBM Corp.
 *
 */
public class Account {

	private String id;
	private String citizenId;
	private String policyNumber;
	private String accountNumber;
	private InsuranceInfo insuranceInfo;
	private MetlifeGroupBenefits metlifeGroupBenefitsInfo;
	private AnnuitiesInfo annuitiesInfo;
	private AnnuitiesInfoMli annuitiesInfoMli;
	private DiabilityIncomeInsurace diabilityIncomeInsuraceInfo;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the citizenId
	 */
	public String getCitizenId() {
		return citizenId;
	}

	/**
	 * @param citizenId
	 *            the citizenId to set
	 */
	public void setCitizenId(String citizenId) {
		this.citizenId = citizenId;
	}

	/**
	 * @return the policyNumber
	 */
	public String getPolicyNumber() {
		return policyNumber;
	}

	/**
	 * @param policyNumber
	 *            the policyNumber to set
	 */
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber
	 *            the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the insuranceInfo
	 */
	public InsuranceInfo getInsuranceInfo() {
		return insuranceInfo;
	}

	/**
	 * @param insuranceInfo
	 *            the insuranceInfo to set
	 */
	public void setInsuranceInfo(InsuranceInfo insuranceInfo) {
		this.insuranceInfo = insuranceInfo;
	}

	/**
	 * @return the metlifeGroupBenefitsInfo
	 */
	public MetlifeGroupBenefits getMetlifeGroupBenefitsInfo() {
		return metlifeGroupBenefitsInfo;
	}

	/**
	 * @param metlifeGroupBenefitsInfo
	 *            the metlifeGroupBenefitsInfo to set
	 */
	public void setMetlifeGroupBenefitsInfo(MetlifeGroupBenefits metlifeGroupBenefitsInfo) {
		this.metlifeGroupBenefitsInfo = metlifeGroupBenefitsInfo;
	}

	/**
	 * @return the annuitiesInfo
	 */
	public AnnuitiesInfo getAnnuitiesInfo() {
		return annuitiesInfo;
	}

	/**
	 * @param annuitiesInfo
	 *            the annuitiesInfo to set
	 */
	public void setAnnuitiesInfo(AnnuitiesInfo annuitiesInfo) {
		this.annuitiesInfo = annuitiesInfo;
	}

	/**
	 * @return the annuitiesInfoMli
	 */
	public AnnuitiesInfoMli getAnnuitiesInfoMli() {
		return annuitiesInfoMli;
	}

	/**
	 * @param annuitiesInfoMli
	 *            the annuitiesInfoMli to set
	 */
	public void setAnnuitiesInfoMli(AnnuitiesInfoMli annuitiesInfoMli) {
		this.annuitiesInfoMli = annuitiesInfoMli;
	}

	/**
	 * @return the diabilityIncomeInsuraceInfo
	 */
	public DiabilityIncomeInsurace getDiabilityIncomeInsuraceInfo() {
		return diabilityIncomeInsuraceInfo;
	}

	/**
	 * @param diabilityIncomeInsuraceInfo
	 *            the diabilityIncomeInsuraceInfo to set
	 */
	public void setDiabilityIncomeInsuraceInfo(DiabilityIncomeInsurace diabilityIncomeInsuraceInfo) {
		this.diabilityIncomeInsuraceInfo = diabilityIncomeInsuraceInfo;
	}
}
