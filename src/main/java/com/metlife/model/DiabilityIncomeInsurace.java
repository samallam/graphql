/*
 * @(#) com.metlife.model.DiabilityIncomeInsurace.java
 * ===========================================================================
 * This source code is exclusive propriety of the  Finances. 
 * In no case are the contents allowed to be distributed to third parties or made public 
 * without prior and written consent of Finances.
 * ===========================================================================
 *  
 */
/*
 * Change Activity:
 *
 * Reason    Date       Author         Version     Description
 * ------    ----       ------      -------     -------------------------------------- 
 * created  10-Feb-2016 Amit Mishra       0.1       created the artifact
 * 
 */
package com.metlife.model;

/**
 * <code> DiabilityIncomeInsurace <code> class
 * 
 * @version 0.1
 * @author Amit Mishra , IBM Corp.
 *
 */
public class DiabilityIncomeInsurace {
	private String id;
	private String OmniAdvantage;
	private String monthlyBenefit;
	private String monthlyBenefitDate;
	private String lumpSum;
	private String lumpSumDate;
	private String prePayDue;
	private String prePayDueDate;
	private String billingOption;
	private String insured;
	private String status;
	private String priBeneficiaries;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the omniAdvantage
	 */
	public String getOmniAdvantage() {
		return OmniAdvantage;
	}

	/**
	 * @param omniAdvantage
	 *            the omniAdvantage to set
	 */
	public void setOmniAdvantage(String omniAdvantage) {
		OmniAdvantage = omniAdvantage;
	}

	/**
	 * @return the monthlyBenefit
	 */
	public String getMonthlyBenefit() {
		return monthlyBenefit;
	}

	/**
	 * @param monthlyBenefit
	 *            the monthlyBenefit to set
	 */
	public void setMonthlyBenefit(String monthlyBenefit) {
		this.monthlyBenefit = monthlyBenefit;
	}

	/**
	 * @return the lumpSum
	 */
	public String getLumpSum() {
		return lumpSum;
	}

	/**
	 * @param lumpSum
	 *            the lumpSum to set
	 */
	public void setLumpSum(String lumpSum) {
		this.lumpSum = lumpSum;
	}

	/**
	 * @return the billingOption
	 */
	public String getBillingOption() {
		return billingOption;
	}

	/**
	 * @param billingOption
	 *            the billingOption to set
	 */
	public void setBillingOption(String billingOption) {
		this.billingOption = billingOption;
	}

	/**
	 * @return the insured
	 */
	public String getInsured() {
		return insured;
	}

	/**
	 * @param insured
	 *            the insured to set
	 */
	public void setInsured(String insured) {
		this.insured = insured;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the priBeneficiaries
	 */
	public String getPriBeneficiaries() {
		return priBeneficiaries;
	}

	/**
	 * @param priBeneficiaries
	 *            the priBeneficiaries to set
	 */
	public void setPriBeneficiaries(String priBeneficiaries) {
		this.priBeneficiaries = priBeneficiaries;
	}

	/**
	 * @return the monthlyBenefitDate
	 */
	public String getMonthlyBenefitDate() {
		return monthlyBenefitDate;
	}

	/**
	 * @param monthlyBenefitDate
	 *            the monthlyBenefitDate to set
	 */
	public void setMonthlyBenefitDate(String monthlyBenefitDate) {
		this.monthlyBenefitDate = monthlyBenefitDate;
	}

	/**
	 * @return the lumpSumDate
	 */
	public String getLumpSumDate() {
		return lumpSumDate;
	}

	/**
	 * @param lumpSumDate
	 *            the lumpSumDate to set
	 */
	public void setLumpSumDate(String lumpSumDate) {
		this.lumpSumDate = lumpSumDate;
	}

	/**
	 * @return the prePayDue
	 */
	public String getPrePayDue() {
		return prePayDue;
	}

	/**
	 * @param prePayDue
	 *            the prePayDue to set
	 */
	public void setPrePayDue(String prePayDue) {
		this.prePayDue = prePayDue;
	}

	/**
	 * @return the prePayDueDate
	 */
	public String getPrePayDueDate() {
		return prePayDueDate;
	}

	/**
	 * @param prePayDueDate
	 *            the prePayDueDate to set
	 */
	public void setPrePayDueDate(String prePayDueDate) {
		this.prePayDueDate = prePayDueDate;
	}
}
