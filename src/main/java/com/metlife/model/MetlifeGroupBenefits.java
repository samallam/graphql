/*
 * @(#) com.metlife.model.MetlifeGroupBenefits.java
 * ===========================================================================
 * This source code is exclusive propriety of the  Finances. 
 * In no case are the contents allowed to be distributed to third parties or made public 
 * without prior and written consent of Finances.
 * ===========================================================================
 *  
 */
/*
 * Change Activity:
 *
 * Reason    Date       Author         Version     Description
 * ------    ----       ------      -------     -------------------------------------- 
 * created  10-Feb-2016 Amit Mishra       0.1       created the artifact
 * 
 */
package com.metlife.model;

/**
 * <code> MetlifeGroupBenefits <code> class
 * 
 * @version 0.1
 * @author Amit Mishra , IBM Corp.
 *
 */
public class MetlifeGroupBenefits {
	private String id;
	private String guarLevelTerm;
	private String faceAmount;
	private String faceAmountDate;
	private String deathBenefit;
	private String deathBenefitDate;
	private String prePayDue;
	private String prePayDueDate;
	private String billingOption;
	private String insured;
	private String status;
	private String priBeneficiaries;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the guarLevelTerm
	 */
	public String getGuarLevelTerm() {
		return guarLevelTerm;
	}

	/**
	 * @param guarLevelTerm
	 *            the guarLevelTerm to set
	 */
	public void setGuarLevelTerm(String guarLevelTerm) {
		this.guarLevelTerm = guarLevelTerm;
	}

	/**
	 * @return the faceAmount
	 */
	public String getFaceAmount() {
		return faceAmount;
	}

	/**
	 * @param faceAmount
	 *            the faceAmount to set
	 */
	public void setFaceAmount(String faceAmount) {
		this.faceAmount = faceAmount;
	}

	/**
	 * @return the faceAmountDate
	 */
	public String getFaceAmountDate() {
		return faceAmountDate;
	}

	/**
	 * @param faceAmountDate
	 *            the faceAmountDate to set
	 */
	public void setFaceAmountDate(String faceAmountDate) {
		this.faceAmountDate = faceAmountDate;
	}

	/**
	 * @return the deathBenefit
	 */
	public String getDeathBenefit() {
		return deathBenefit;
	}

	/**
	 * @param deathBenefit
	 *            the deathBenefit to set
	 */
	public void setDeathBenefit(String deathBenefit) {
		this.deathBenefit = deathBenefit;
	}

	/**
	 * @return the deathBenefitDate
	 */
	public String getDeathBenefitDate() {
		return deathBenefitDate;
	}

	/**
	 * @param deathBenefitDate
	 *            the deathBenefitDate to set
	 */
	public void setDeathBenefitDate(String deathBenefitDate) {
		this.deathBenefitDate = deathBenefitDate;
	}

	/**
	 * @return the prePayDue
	 */
	public String getPrePayDue() {
		return prePayDue;
	}

	/**
	 * @param prePayDue
	 *            the prePayDue to set
	 */
	public void setPrePayDue(String prePayDue) {
		this.prePayDue = prePayDue;
	}

	/**
	 * @return the prePayDueDate
	 */
	public String getPrePayDueDate() {
		return prePayDueDate;
	}

	/**
	 * @param prePayDueDate
	 *            the prePayDueDate to set
	 */
	public void setPrePayDueDate(String prePayDueDate) {
		this.prePayDueDate = prePayDueDate;
	}

	/**
	 * @return the billingOption
	 */
	public String getBillingOption() {
		return billingOption;
	}

	/**
	 * @param billingOption
	 *            the billingOption to set
	 */
	public void setBillingOption(String billingOption) {
		this.billingOption = billingOption;
	}

	/**
	 * @return the insured
	 */
	public String getInsured() {
		return insured;
	}

	/**
	 * @param insured
	 *            the insured to set
	 */
	public void setInsured(String insured) {
		this.insured = insured;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the priBeneficiaries
	 */
	public String getPriBeneficiaries() {
		return priBeneficiaries;
	}

	/**
	 * @param priBeneficiaries
	 *            the priBeneficiaries to set
	 */
	public void setPriBeneficiaries(String priBeneficiaries) {
		this.priBeneficiaries = priBeneficiaries;
	}

}
